import { Meteor } from 'meteor/meteor';
import {AWS} from 'meteor/peerlibrary:aws-sdk';
import { Preferences } from '../imports/api/preferences';

Meteor.startup(() => {
  // code to run on server at startup
  Meteor.methods({

    'createBucket'({ps}) {
         let credentials = new AWS.SharedIniFileCredentials({profile:ps.endpoints[0].name});
         AWS.config.credentials = credentials;
         //create a s3 client.
         let s3 = new AWS.S3();
         s3.createBucket({Bucket: ps.endpoints[0].rootPath},
            Meteor.bindEnvironment((err, data) => {
                if (err) console.log(err, err.stack); // an error occurred
                else{
                    console.log('success Create Bucket!');
                    Preferences.insert({
                        id: ps.id,
                        endpoints: ps.endpoints
                    });
                }
            })
        )
    },

    'listObjects'({endpoint}){
        let credentials = new AWS.SharedIniFileCredentials({profile:endpoint.name});
        AWS.config.credentials = credentials;
        //create a s3 client.
        let s3 = new AWS.S3();

        return new Promise(
          (resolve,reject) => {
            s3.listObjectsV2(
              {Bucket: endpoint.rootPath},
              Meteor.bindEnvironment(
                (err, res) => {
                  if (err) console.log(err, err.stack); // an error occurred
                  else {
                    resolve(res.Contents);
                  }
              })
          )}
        );
    }

  })
})
