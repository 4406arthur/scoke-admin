import React, { Component, PropTypes } from 'react';
import { Meteor } from 'meteor/meteor';
import { createContainer } from 'meteor/react-meteor-data';
import ReactDOM from 'react-dom';
import '../stylesheets/form-mini.css';
//mongo client.
import { Preferences } from '../api/preferences.js'
//ui sub component
import Preference from './Preference.jsx';


// App component - represents the whole app
class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      showDefault: false,
      filteredData: false
    };
  }

  handleSubmit(event) {
    event.preventDefault();
    // Find the text field via the React ref
    const pid = ReactDOM.findDOMNode(this.refs.pid).value.trim();
    const name = ReactDOM.findDOMNode(this.refs.epName).value.trim();
    const region = ReactDOM.findDOMNode(this.refs.epRegion).value.trim();
    const url = ReactDOM.findDOMNode(this.refs.epUrl).value.trim();
    const signature = ReactDOM.findDOMNode(this.refs.epSignature).value.trim();
    const bucket = ReactDOM.findDOMNode(this.refs.epBucket).value.trim();

    let endpoint = {
        name: name,
        rootPath: bucket,
        url: url,
        region: region,
        signatureVersion: signature,
        isDefault: true
    };

    let ps = {
      id: pid,
      endpoints: [endpoint]
    };

    Meteor.call("createBucket",{ ps },
    (err, res) => {
      if (err) {
        alert(err);
      } else {
        // Clear form
        ReactDOM.findDOMNode(this.refs.pid).value = '';
        ReactDOM.findDOMNode(this.refs.epName).value = '';
        ReactDOM.findDOMNode(this.refs.epBucket).value = '';
        ReactDOM.findDOMNode(this.refs.epUrl).value = '';
        ReactDOM.findDOMNode(this.refs.epRegion).value = '';
        ReactDOM.findDOMNode(this.refs.epSignature).value = '';
      }
    });
  }

  searchHandler(event) {
    event.preventDefault();

    const clientID = ReactDOM.findDOMNode(this.refs.textInput).value.trim();

    let filteredPreferences = this.props.Preferences;
    filteredPreferences = filteredPreferences.filter(preference => preference.id == clientID
    );

    this.setState({
      filteredData: filteredPreferences
    })
  }

  renderPreferences() {
    if (this.state.filteredData.length === 1) {
      return this.state.filteredData.map((preference) => (
        <Preference
          key={preference._id}
          preference={preference}
        />
      ));
    }

    //else list all preferences
    /*return this.props.Preferences.map((preference) => (
      <Preference
      key={preference._id}
      preference={preference}
      />
    ));*/
  }

  render() {
    return (
      <div className="container">
        <header>
          <h2>ST Preference LookUp</h2>
              <form className="search-task" onSubmit={this.searchHandler.bind(this)} >
                <input type="text" ref="textInput" placeholder="search with ClientID"/>
              </form>
              {this.renderPreferences()}
        </header>
        <div className="d3-graph" id="d3" width="600" height="100%"></div>
          <div className="form-mini-container">
            <p/>
            <h1>Create Preference </h1>
              <form className="form-mini" onSubmit={this.handleSubmit.bind(this)} >
                <div className="form-row">
                    <input type="text" ref="pid" placeholder="Your Preference Id"/>
                </div>
                <div className="form-row">
                    <input type="text" ref="epName" placeholder="Assign Storage endpoint Name"/>
                    <input type="text" ref="epUrl" placeholder="Assogn Storage endpoint Url"/>
                    <input type="text" ref="epRegion" placeholder="Assgin Endpoint Region"/>
                    <input type="text" ref="epSignature" placeholder="Assign Signature Version"/>
                    <input type="text" ref="epBucket" placeholder="Assgin Bucket Name"/>
                </div>
                <div className="form-row form-last-row">
                    <button type="submit">Submit Form</button>
                </div>
              </form>
          </div>
      </div>
    );
  }

}

App.propTypes = {
  Preferences: PropTypes.array.isRequired,
//incompleteCount: PropTypes.number.isRequired
};

export default createContainer(() => {
  return {
    Preferences: Preferences.find({}).fetch(),
  //incompleteCount: Preferences.find({ checked: { $ne: true } }).count(),
  };
}, App);
