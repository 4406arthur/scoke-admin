import React, { Component, PropTypes } from 'react';
import { Meteor } from 'meteor/meteor';
import * as d3 from "d3";

export default class Endpoint extends Component {
  usageHandler(){

    p1 = new Promise((resolve, reject) => {
      Meteor.call("listObjects", this.props,
        (err, res) => {
          if (err) alert(err);
          else resolve(res);
        }
      );
    });

    //transform array into d3 format
    p1.then((resolve)=>{

      let data = resolve.reduce((prev,current)=>{
        var n = current.Key.lastIndexOf('/');
        var fileName = current.Key.substring(n + 1);
        var dir = current.Key.substring(0,n);
        var obj = {
           name : fileName,
           size : current.Size
        };

        var found = prev.find((element,index,array)=>{
          if(element.name === dir){
            array[index].children.push(obj);
            return true;
          }
          return false;
        });

        if(!found)
        {
          prev.push({
            name : dir,
            children: [obj]
          });
        }
        return prev;
      },[]);

      //d3 initial
      document.getElementById('d3').innerHTML = "";
      var diameter = 600,
      format = d3.format(",d"),
      color = d3.scaleOrdinal(d3.schemeCategory20c);

      var bubble = d3.pack()
          .size([diameter, diameter])
          .padding(1.5);

      var svg = d3.select("#d3").append("svg")
          .attr("width", diameter)
          .attr("height", diameter)
          .attr("class", "bubble");

      var root = d3.hierarchy(classes({children:data}))
        .sum(function(d) { return d.value; })
        .sort(function(a, b) { return b.value - a.value; });

      bubble(root);

      var node = svg.selectAll(".node")
          .data(root.children)
        .enter().append("g")
          .attr("class", "node")
          .attr("transform", function(d) { return "translate(" + d.x + "," + d.y + ")"; });

      node.append("title")
          .text(function(d) { return d.data.className + ": " + format(d.value); });

      node.append("circle")
          .attr("r", function(d) { return d.r; })
          .style("fill", function(d) {
            return color(d.data.packageName);
          });

      node.append("text")
          .attr("dy", ".3em")
          .style("text-anchor", "middle")
          .text(function(d) { return d.data.className.substring(0, d.r / 3); });

      // Returns a flattened hierarchy containing all leaf nodes under the root.
      function classes(root) {
        var classes = [];

        function recurse(name, node) {
          if (node.children) node.children.forEach(function(child) { recurse(node.name, child); });
          else classes.push({packageName: name, className: node.name, value: node.size});
        }

        recurse(null, root);
        return {children: classes};
      }
    });

  }

  render() {
    return (
      <li className="private">
        <span className="text">
          <strong>{this.props.endpoint.name}</strong>
        </span>
        <p/>
        <span className="sub">
          Bucket:{this.props.endpoint.rootPath}
        </span>
        <button
          type="button"
          className="btn btn-info pull-right"
          onClick={this.usageHandler.bind(this)}
          >
          Usage
        </button>
      </li>
    );
  }
}

Endpoint.propTypes = {
  // We can use propTypes to indicate it is required
  endpoint: PropTypes.object.isRequired,
};
