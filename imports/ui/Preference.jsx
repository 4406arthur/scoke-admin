import React, { Component, PropTypes } from 'react';

//sub ui.
import Endpoint from './Endpoint.jsx';

// Preference component - represents Preference component
export default class Preference extends Component {
  constructor(props) {
    super(props);
    this.state = {
      //for default endpoint use.
      showDefault: false,
    };
  }

  toggleShowDefault() {
    this.setState({
      showDefault: !this.state.showDefault,
    });
  }

  showEndpoints() {
    if(this.state.showDefault) {
        let defaultEndpoint = this.props.preference.endpoints;
        defaultEndpoint = defaultEndpoint.filter(
          endpoint => endpoint.isDefault);

        return defaultEndpoint.map((endpoint) => (
          <Endpoint
            key={endpoint.name}
            endpoint={endpoint}
          />
        ));
    }

    return this.props.preference.endpoints.map((endpoint)=>(
      <Endpoint
         key={endpoint.name}
         endpoint={endpoint}
      />
    ));
  }

  render() {
    return (
      <ul>
          <span className="text">
            <input
              type="checkbox"
              onClick={this.toggleShowDefault.bind(this)}
            />ShowDefault
          </span>
          <p/>
          {this.showEndpoints()}
      </ul>
    );
  }
}

Preference.propTypes = {
  // This component gets the task to display through a React prop.
  // We can use propTypes to indicate it is required
  preference: PropTypes.object.isRequired,
};
